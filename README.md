# Verificator of iCommunity Evidences
> [*Note: execute commands at the project directory*]
## Getting Started
> [*Note: Aplication developed on node v.14.17.6 using npm v.6.14.15*]
- Install dependencies
<pre>npm install</pre>
- Create .env file
<pre>touch .env</pre>
- Writte inside .env file the RPCs addresses for connect to Blockchains
> [*Note: Values provided here works at 23th may 2023*]
<pre>
REACT_APP_IPFS_URL="https://icommunity.infura-ipfs.io/ipfs"
REACT_APP_MUMBAI_RPC="https://rpc-mumbai.maticvigil.com"
REACT_APP_MATIC_RPC="https://rpc.ankr.com/polygon"
REACT_APP_CHIADO_RPC="https://rpc.chiadochain.net"
REACT_APP_XDAI_RPC="https://rpc.gnosischain.com"
REACT_APP_BSC_TESTNET_RPC="https://data-seed-prebsc-1-s1.binance.org:8545"
REACT_APP_BSC_MAINNET_RPC="https://bsc-dataseed.binance.org"
REACT_APP_AVALANCHE_FUJI_TESTNET_RPC="https://ava-testnet.public.blastapi.io/ext/bc/C/rpc"
REACT_APP_AVALANCHE_CCHAIN_MAINNET_RPC="https://ava-mainnet.public.blastapi.io/ext/bc/C/rpc"
REACT_APP_FANTOM_TESTNET_RPC="https://rpc.testnet.fantom.network"
REACT_APP_FANTOM_OPERA_MAINNET_RPC="https://rpc.fantom.network"
REACT_APP_ARBITRUM_GOERLI_TESTNET_RPC="https://goerli-rollup.arbitrum.io/rpc"
REACT_APP_ARBITRUM_ONE_MAINNET_RPC="https://arb1.arbitrum.io/rpc"
REACT_APP_ETHEREUM_GOERLI_TESTNET_RPC="https://rpc.ankr.com/eth_goerli"
REACT_APP_ETHEREUM_MAINNET_RPC="https://rpc.ankr.com/eth"
GENERATE_SOURCEMAP=false
</pre>

## Executing with node
- Execute
> [*Note: Default port: 3000*]
<pre>npm start</pre>
- Visualize on your browser
> [*Note: Default URL http://localhost:3000*]

## Deploy on a static server
- On package.json file adjust this line of code to match your path
    - With relative path (Recommended)
    <pre>"homepage": "./",</pre>
    - With absolute path (domain address + folder)
    <pre>"homepage": "https://yourwebsite.com/your_folder_name/",</pre>
- Execute
<pre>npm run build</pre>
- Take all files from that newly created **build** folder and upload them into your_folder_name
- Visualize on your browser
