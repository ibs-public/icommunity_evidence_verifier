(function (window) {
  window["env"] = window["env"] || {};
  window["env"]["REACT_APP_IPFS_URL"] = "${REACT_APP_IPFS_URL}";
  window["env"]["REACT_APP_MUMBAI_RPC"] = "${REACT_APP_MUMBAI_RPC}";
  window["env"]["REACT_APP_MATIC_RPC"] = "${REACT_APP_MATIC_RPC}";
  window["env"]["REACT_APP_CHIADO_RPC"] = "${REACT_APP_CHIADO_RPC}";
  window["env"]["REACT_APP_XDAI_RPC"] = "${REACT_APP_XDAI_RPC}";
  window["env"]["REACT_APP_ETHEREUM_GOERLI_TESTNET_RPC"] = "${REACT_APP_ETHEREUM_GOERLI_TESTNET_RPC}";
  window["env"]["REACT_APP_ETHEREUM_MAINNET_RPC"] = "${REACT_APP_ETHEREUM_MAINNET_RPC}";
  window["env"]["REACT_APP_BNB_TESTNET_RPC"] = "${REACT_APP_BNB_TESTNET_RPC}";
  window["env"]["REACT_APP_BNB_MAINNET_RPC"] = "${REACT_APP_BNB_MAINNET_RPC}";
  window["env"]["REACT_APP_AVALANCHE_FUJI_TESTNET_RPC"] = "${REACT_APP_AVALANCHE_FUJI_TESTNET_RPC}";
  window["env"]["REACT_APP_AVALANCHE_CCHAIN_MAINNET_RPC"] = "${REACT_APP_AVALANCHE_CCHAIN_MAINNET_RPC}";
  window["env"]["REACT_APP_FANTOM_TESTNET_RPC"] = "${REACT_APP_FANTOM_TESTNET_RPC}";
  window["env"]["REACT_APP_FANTOM_OPERA_MAINNET_RPC"] = "${REACT_APP_FANTOM_OPERA_MAINNET_RPC}";
  window["env"]["REACT_APP_ARBITRUM_GOERLI_TESTNET_RPC"] = "${REACT_APP_ARBITRUM_GOERLI_TESTNET_RPC}";
  window["env"]["REACT_APP_ARBITRUM_ONE_MAINNET_RPC"] = "${REACT_APP_ARBITRUM_ONE_MAINNET_RPC}";
  //console.log('environment vars defined on window.env',window.env);
})(this);

const process = window;
