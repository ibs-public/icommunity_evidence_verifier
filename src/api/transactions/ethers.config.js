// NETWORKS
import networks from "networks/config";

// HELPERS
const { Chronometer } = require("helpers/utils/Chronometer");
const {sleep} = require("helpers/utils/sleep")

// MAIN
export const get_Transaction= async (blockchain_network, tx_hash) => {
  try {
    let provider = await networks(blockchain_network).provider();
    
    let transaction= null
    await Promise.race([
      Chronometer(1.5 * 60 * 1000, "Blockchain temporally blocked"), // Espera 1.5 minutos
      (async () => {
        let count =0
        while(transaction===null && count<3){
          transaction = await provider.getTransaction(tx_hash);
          sleep(2000)
          count++
        }  
      })(),
    ]);
  return { status: 1, message: transaction }    
  } catch (error) {
    console.log(error);
    return { status: 0, message: error.toString() };
  }
}


