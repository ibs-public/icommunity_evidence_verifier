import { get_Transaction } from "./ethers.config";

// Get Transaction Method
const getTransaction = (network, hash) => get_Transaction(network, hash);

export { getTransaction };
