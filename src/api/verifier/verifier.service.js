import { verifier } from "./crypto.config";

// Get Receipt Method
const verify = (data, integrity) => verifier(data, integrity);

export { verify };