import crypto from "crypto-browserify";
import { fileToBinaryBuffer } from "helpers/utils/fileToBinaryBuffer";

export const verifier = async (data, integrity) => {
  try {
    let response = false;
    let checksums = [];
    let algorithm = null;
    if ((data.text && data.text !== "") || (data.files && data.files.length > 0)) {
      response = true;
      let integrities = JSON.parse(integrity);
      for (let index in integrities) {
        if (
          integrities[index].checksum !==
          "z4PhNX7vuL3xVChQ1m2AB9Yg5AULVxXcg/SpIdNs6c5H0NE8XYXysP+DGNKHfuwvY7kxvUdBeoGlODJ6+SfaPg=="
        ) {
          checksums.push(integrities[index].checksum);
          if (!algorithm) {
            switch (integrities[index].algorithm) {
              case "md5":
                algorithm = "md5";
                break;
              case "SHA-512":
                algorithm = "sha512";
                break;
              default:
                algorithm = null;
            }
            console.log("algorithm", algorithm);
          }
        }
      }
    }
    if (data.text && data.text !== "") {
      let hash = crypto.createHash(algorithm);
      hash.update(data.text);
      const textHash = hash.digest("base64");
      if (!checksums.includes(textHash)) {
        response = false;
      } else {
        checksums.splice(checksums.indexOf(textHash), 1);
      }
    }

    if (data.files && data.files.length > 0) {
      for (let i = 0; i < data.files.length; i++) {
        let buffer = await fileToBinaryBuffer(data.files[i].file);
        let hash = crypto.createHash(algorithm);
        hash.update(buffer);
        const fileHash = hash.digest("base64");
        if (!checksums.includes(fileHash)) {
          response = false;
        } else {
          checksums.splice(checksums.indexOf(fileHash), 1);
        }
      }
    }

    return { status: 1, data: response };
  } catch (error) {
    console.log(error);
    return { status: -1, data: "" };
  }
};
