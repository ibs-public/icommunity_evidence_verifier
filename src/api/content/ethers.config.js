// ETHERS
import { ethers } from "ethers";

// NETWORKS
import networks from "networks/config";

// HELPERS
const { BytesHex_To_String } = require("helpers/utils/BytesHex_To_String");
const { Chronometer } = require("helpers/utils/Chronometer");
const {sleep} = require("helpers/utils/sleep")

export const get_Certification_Content = async (blockchain_network, tx_hash) => {
  try {
    let provider = await networks(blockchain_network).provider();
    let Transaction= null
    await Promise.race([
      Chronometer(1.5 * 60 * 1000, "Blockchain temporally blocked"), // Espera 1.5 minutos
      (async () => {
        let count =0
        while(Transaction===null && count<3){
          Transaction = await provider.getTransaction(tx_hash);
          sleep(2000)
          count++
        }  
      })(),
    ]);
    if (Transaction !== null) {
      let TransactionReceipt 
      await Promise.race([
        Chronometer(1.5 * 60 * 1000, "Blockchain temporally blocked"), // Espera 1.5 minutos
        (async () => {
          TransactionReceipt = await provider.waitForTransaction(tx_hash);
        })(),
      ]);
      if (
        TransactionReceipt &&
        TransactionReceipt.to &&
        networks(blockchain_network).contracts.includes(TransactionReceipt.to)
      ) {   
        const data = await ethers.utils.defaultAbiCoder.decode(
          ["address", "bytes", "bytes", "bytes", "bytes", "bytes", "uint8"],
          TransactionReceipt.logs[0].data
        );
        // Formateamos la informacion
        const TX_data = {
          status: 1,
          useraddress: data[0],
          userIdentity: BytesHex_To_String(data[1]),
          title: BytesHex_To_String(data[2]),
          contentChecksum: BytesHex_To_String(data[3]),
          externalContentAddress: BytesHex_To_String(data[4]),
          metadata: BytesHex_To_String(data[5]),
          version: data[6],
        };
        return TX_data;
      } else {
        return { status: -2, message: "Not iCommunity Transaction" };
      }
    } else {
      return { status: -1, message: "Transaction Not Exist" };
    }
  } catch (error) {
    /* console.log(error); */
    return { status: 0, message: error.toString() };
  }
};
