import { get_Certification_Content } from "./ethers.config";

// Get Receipt Method
const getCertificationContent = (blockchain_network, tx_hash) => get_Certification_Content(blockchain_network, tx_hash);

export { getCertificationContent };
