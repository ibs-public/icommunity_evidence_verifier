//React
import React from 'react';
import ReactDOM from 'react-dom';

// React Router Dom
import { BrowserRouter } from "react-router-dom"

// Redux
import { Provider } from "react-redux"
import store from "./store"

// i18n
import './i18n';

// Components
import App from "./App";

//Styles
import 'bootstrap/dist/css/bootstrap.min.css';
import 'boxicons/css/boxicons.min.css';
import './assets/css/main.css'

//RENDER
const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
)

ReactDOM.render(app, document.getElementById("root"))
