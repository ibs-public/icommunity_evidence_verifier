/** Convert file in a array buffer*/
import { Buffer } from "buffer";
export const fileToBinaryBuffer = async (file) => {
  return new Promise(function (resolve, reject) {
    const reader = new FileReader();

    reader.onerror = function onerror(ev) {
      reject(ev.target.error);
    };

    reader.onload = function onload(ev) {
      resolve(Buffer.from(ev.target.result));
    };

    reader.readAsArrayBuffer(file);
  });
};

export default fileToBinaryBuffer;
