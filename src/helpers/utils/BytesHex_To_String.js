function BytesHex_To_String(bytesHex) {
  let hex = bytesHex.toString();
  let str = "";
  for (let n = 2; n < hex.length; n += 2) {
    str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
  }
  return str;
}

exports.BytesHex_To_String = BytesHex_To_String;
