/** Check if is possible convert string to JSON */
export const isStringValidJSON = (str) => {
  try {
    JSON.parse(str);
    return true;
  } catch (e) {
    return false;
  }
};

export default isStringValidJSON;
