import i18n from "i18next"
import detector from "i18next-browser-languagedetector"
import { initReactI18next } from "react-i18next"

import { LS_I18N } from './helpers/localstorage/definitions'

import translationSP from "./locales/es/translation.json"
import translationENG from "./locales/en/translation.json"

// the translations
const resources = {
  es: {
    translation: translationSP,
  },
  en: {
    translation: translationENG,
  },
}

const language = localStorage.getItem(LS_I18N)
if (!language) {
  localStorage.setItem(LS_I18N, "es")
}

i18n
  .use(detector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng:  localStorage.getItem(LS_I18N)  || "es",
    fallbackLng: "es", // use en if detected lng is not available

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  })

export default i18n
