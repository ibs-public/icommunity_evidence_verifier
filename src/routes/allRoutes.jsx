// React
import React from "react";

// React Router Dom
import { Routes, Route } from "react-router-dom";

// Pages
import Index from "pages/index";
import Checker from "pages/checker";
import Pdf from "pages/pdf";
import Verifier from "pages/verifier";
import NotFound from "components/commons/NotFound";

const userRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Index />} />
      <Route path="/check/:network/:hash" element={<Checker />} />
      <Route path="/pdf/:network/:id" element={<Pdf />} />
      <Route path="/verify/:network/:hash" element={<Verifier />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
};

export default userRoutes;
