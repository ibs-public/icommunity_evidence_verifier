import ukFlag from "assets/images/flags/uk.png";
import spain from "assets/images/flags/spain.jpg";

const languages = {
  es: {
    label: "Spanish",
    flag: spain,
  },
  en: {
    label: "English",
    flag: ukFlag,
  },
};

export default languages;
