// React
import * as React from "react";

// i18n
import { withTranslation } from "react-i18next";

// Layout
import Layout from "components/layouts/Layout";

// Components
import TransactionVerifier from "components/verifier/TransactionVerifier";

function verifier(props) {
  return (
    <Layout>
      <TransactionVerifier />
    </Layout>
  );
}

export default withTranslation()(verifier);
