// React
import * as React from "react";

// i18n
import { withTranslation } from "react-i18next";

// Layout
import Layout from "components/layouts/Layout";

// Components
import EnterHash from "components/checker/enter_hash";

function index(props) {
  return (
    <Layout>
      <EnterHash />
    </Layout>
  );
}

export default withTranslation()(index);
