// React
import * as React from "react";

// i18n
import { withTranslation } from "react-i18next";

// Layout
import Layout from "components/layouts/Layout";

// Components
import CheckHash from "components/checker/check_hash";

function checker(props) {
  return (
    <Layout>
      <CheckHash />
    </Layout>
  );
}

export default withTranslation()(checker);
