// React
import * as React from "react";

// i18n
import { withTranslation } from "react-i18next";

// Layout
import Layout02 from "components/layouts/Layout02";

// Components
import PdfViewer from "components/pdf/pdf_viewer";

function checker(props) {
  return (
    <Layout02>
      <PdfViewer />
    </Layout02>
  );
}

export default withTranslation()(checker);
