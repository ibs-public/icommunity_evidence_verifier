import * as React from "react";
import UserRoutes  from "./routes/allRoutes";

function App() {
  return (
    <div className="App">
      <UserRoutes/>
    </div>
  );
}

export default App;
