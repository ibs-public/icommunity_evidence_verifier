// React
import React, { useEffect, useState } from "react";

// Reactstrap
import { Row, Col, Label } from "reactstrap";

// React Router Dom
import withRouter from "helpers/router/withRouter";
/* import { useParams } from "react-router-dom"; */

// i18n
import { withTranslation } from "react-i18next";

// Networks
import { list_Networks, list_Networks_Strings, list_Networks_Type } from "networks";
import networks from "networks/config";

const Certification = (props) => {
  const params = props.router.params;
  /* const [params, setParams] = useState(useParams()); */
  const [hash, setHash] = useState(null);
  const [netname, setNetname] = useState(null);
  //const [slogan, setSlogan] = useState(null);

  useEffect(() => {
    if (params && params.hash && params.network) {
      setHash(params.hash);
      setNetname(networks(params.network).name);
      //setSlogan(networks(params.network).slogan);
    }
  }, [params]);

  return (
    <React.Fragment>
      <section id="nocertification">
        <Row>
          <Col className="d-flex justify-content-center align-items-center">
            <i className="bx bx-x-circle rojo icono-grande"></i>
            <span className="text36">{props.t("ERROR")}</span>
          </Col>
        </Row>
        <Row className="mt-3 mb-3 network-logos d-flex justify-content-center">
          {Object.entries(list_Networks).map((w, j) => (
            <React.Fragment key={j}>
              {list_Networks_Strings(w[1]).includes("MainNet") && (
                <Col lg="4" md="6" sm="12" className="d-flex justify-content-center mb-4">
                  <img
                    className={`img img-fluid  ${
                      networks(params.network).blockchain === networks(list_Networks_Type(w[1])).blockchain
                        ? "img-heighlight"
                        : "soft"
                    }`}
                    style={{ maxHeight: "60px" }}
                    src={networks(list_Networks_Type(w[1])).emblem}
                    alt={list_Networks_Type(w[1])}
                    title=""
                  />
                </Col>
              )}
            </React.Fragment>
          ))}
        </Row>
        <Row className="pt-3">
          <Col lg="12">
            <p className="textcentered text14">
              <strong>
                {props.t("Secure-not-registered")}:
                <br />
                {`${netname}`.toUpperCase()}
              </strong>
            </p>
            {/* <p className="textcentered text14">{props.t(slogan)}</p> */}
          </Col>
        </Row>

        <Row className="pt-3">
          <Col lg="3"></Col>
          <Col lg="6">
            <Row className="bg-danger btn-rounded">
              <Col lg="10" className="d-flex justify-content-center p-2">
                <span className="text-white text16">{props.t("hash-not-verified")}</span>
              </Col>
              <Col lg="2" className="d-flex justify-content-center">
                <i className="bx bx-error text-white icono-medio"></i>
              </Col>
            </Row>
          </Col>
          <Col lg="3"></Col>
        </Row>
        <br />
        <Row>
          <Col lg="12">
            <p className="text14">
              {props.t("transaction-id")}:
              <br />
              <Label className="text-danger" style={{ wordBreak: "break-word" }}>
                {hash}
              </Label>
            </p>
          </Col>
        </Row>
      </section>
    </React.Fragment>
  );
};

export default withRouter(withTranslation()(Certification));
