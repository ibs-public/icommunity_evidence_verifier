// React
import React, { useEffect, useState } from "react";
import { Alert } from "reactstrap";

// React Router Dom
import withRouter from "helpers/router/withRouter";
import { Navigate } from "react-router-dom";

// Redux
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Actions
import { getTransaction } from "store/actions";

//Loader
import Loader from "../commons/Loader";

// Components
import Certification from "./certification";
import NoCertification from "./no_certification";

// Networks
import { list_Networks } from "networks";

// i18n
import { withTranslation } from "react-i18next";

const Consulta = (props) => {
  // Functions from props
  const { onGetTransaction } = props.R_Functions;

  // Stats frop props
  const { TransactionData, TransactionSuccess, TransactionError, TransactionLoading } = props.RC_Transactions;
  const { network, hash } = props.router.params;

  // Local States
  const [certificated, setCertificated] = useState(false);
  const [checked, setChecked] = useState(false);
  const [networkNotFound, setNetworkNotFound] = useState(false);
  const [contract, setContract] = useState("");

  // UseEffects
  useEffect(() => {
    if (hash && network) {
      if (network !== "") {
        let networkValid = false;
        Object.entries(list_Networks).forEach(([key, value]) => {
          if (network === value) {
            networkValid = true;
          }
        });
        if (networkValid) {
          onGetTransaction(network, hash);
        } else {
          setNetworkNotFound(true);
        }
      }
    }
  }, [hash, network]);

  useEffect(() => {
    if (TransactionSuccess === true) {
      setChecked(true);
      if (TransactionData && TransactionData.hash !== "inexistente") {
        setCertificated(true);
        setContract(TransactionData.to);
      } else {
        setCertificated(false);
      }
    }
  }, [TransactionSuccess]);

  // RENDER
  return (
    <React.Fragment>
      <section id="hashchecker" className="container-fluid">
        {networkNotFound ? (
          <Navigate to="*" />
        ) : (
          <>
            {TransactionLoading ? (
              <Loader />
            ) : (
              <>
                {TransactionError && (
                  <Alert color="danger" className="text-center" style={{ marginTop: "13px" }}>
                    <p className="mt-2">{props.t("MsgErr[Generical error]")}</p>
                    <a href={"mailto:development@icommunity.com"}>development@icommunity.com</a>
                    <p className="mt-4">{props.t("SorryForTheInconvenience")}</p>
                  </Alert>
                )}

                {checked && (
                  <>
                    {certificated ? (
                      <Certification hash={hash} network={network} contract={contract} />
                    ) : (
                      <NoCertification hash={hash} network={network} />
                    )}
                  </>
                )}
              </>
            )}
          </>
        )}
      </section>
    </React.Fragment>
  );
};

Consulta.propTypes = {
  RC_Transactions: PropTypes.any,
  R_Functions: PropTypes.any,
};

const mapStatetoProps = (state) => {
  return {
    RC_Transactions: state.Transaction,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onGetTransaction: (network, hash) => dispatch(getTransaction(network, hash)),
  },
});

export default withRouter(connect(mapStatetoProps, mapDispatchToProps)(withTranslation()(Consulta)));
