import React, { useState, useEffect } from "react";
import { Row, Col, Form, FormGroup, Input, Label, Button } from "reactstrap";

// React Router Dom
import { Navigate } from "react-router-dom";

// i18n
import { withTranslation } from "react-i18next";

// Networks
import { list_Networks, list_Networks_Strings, list_Networks_Type } from "networks";
import networks from "networks/config";

const EnterHash = (props) => {
  const [hash, setHash] = useState("");
  const [network, setNetwork] = useState("select");
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const [send, setSend] = useState(false);

  useEffect(() => {
    if (hash.length === 66 && network !== "select") {
      setButtonDisabled(false);
    } else {
      setButtonDisabled(true);
    }
  }, [network, hash]);

  const handleChangeHash = (event) => {
    setHash(event.target.value);
  };

  const handlechangeSelect = (event) => {
    setNetwork(event.target.value);
  };
  const handlechangeSelectFromImage = (event) => {
    if (network !== event.target.id) {
      setNetwork(event.target.id);
    }
  };
  const handleCheckTransaction = (event) => {
    setSend(true);
  };

  return (
    <React.Fragment>
      {send ? (
        <Navigate to={`check/${network}/${hash}`} />
      ) : (
        <section id="EnterHash">
          <Row>
            <Col className="d-flex justify-content-center">
              <span id="certificado" className="titulo-main">
                <span>{props.t("Checker")}</span>
              </span>
            </Col>
          </Row>
          <Row className="mt-3 mb-5 network-logos d-flex justify-content-center">
            {Object.entries(list_Networks).map((w, j) => (
              <React.Fragment key={j}>
                {list_Networks_Strings(w[1]).includes("MainNet") && (
                  <Col lg="4" md="6" sm="12" className="d-flex justify-content-center mb-4">
                    <img
                      className={`img img-fluid  ${
                        networks(network) &&
                        networks(network).blockchain !== networks(list_Networks_Type(w[1])).blockchain
                          ? "soft"
                          : "img-heighlight"
                      }`}
                      style={{ maxHeight: "60px", cursor: "pointer" }}
                      src={networks(list_Networks_Type(w[1])).emblem}
                      alt={list_Networks_Type(w[1])}
                      title=""
                      id={list_Networks_Type(w[1])}
                      onClick={handlechangeSelectFromImage}
                    />
                  </Col>
                )}
              </React.Fragment>
            ))}
          </Row>
          <Row className="pt-3">
            <Col>
              <Form className="form-group">
                <Row className="mb-md-3 mb-sm-3">
                  <Col lg="4">
                    <FormGroup>
                      <Label for="Select">{props.t("Network")}:</Label>
                      <Input
                        id="Select"
                        name="select"
                        type="select"
                        className={list_Networks_Strings(network).includes("MainNet") ? "font-weight-bold" : ""}
                        onChange={handlechangeSelect}
                        value={network}
                      >
                        {network === "select" && <option value="select">{props.t("Select")}</option>}
                        {Object.entries(list_Networks).map((w, j) => (
                          <option
                            value={list_Networks_Type(w[1])}
                            className={list_Networks_Strings(w[1]).includes("MainNet") ? "font-weight-bold" : ""}
                            key={j}
                          >
                            {list_Networks_Strings(w[1])}
                          </option>
                        ))}
                      </Input>
                    </FormGroup>
                  </Col>
                  <Col lg="8">
                    <FormGroup>
                      <Label for="hash">{props.t("transaction-id")}</Label>
                      <Input name="hash" type="text" onChange={handleChangeHash} value={hash} />
                    </FormGroup>
                  </Col>
                </Row>

                <Row className="pt-md-5 pt-sm-4">
                  <Col className="d-flex justify-content-center">
                    <Button
                      color="primary"
                      className="btn btn-rounded btn-primary btn-block waves-effect waves-light"
                      type="button"
                      disabled={buttonDisabled}
                      onClick={handleCheckTransaction}
                    >
                      {props.t("Submit")}
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        </section>
      )}
    </React.Fragment>
  );
};

export default withTranslation()(EnterHash);
