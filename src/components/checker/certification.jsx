// React
import React, { useEffect, useState } from "react";

// Reactstrap
import { Row, Col, Label, Alert } from "reactstrap";

// React Router Dom
import withRouter from "helpers/router/withRouter";
import { Link } from "react-router-dom";

// Redux
import { connect } from "react-redux";
import store from "store";

// Actions
import { getTransactionContent } from "store/actions";

// Loader
import Loader2 from "../commons/Loader2";

// i18n
import { withTranslation } from "react-i18next";

// PDF
import { PDFDownloadLink } from "@react-pdf/renderer";
import PdfDocument from "components/pdf/pdf_document";

//isMobile
import { isMobile } from "react-device-detect";

// Networks
import { list_Networks, list_Networks_Strings, list_Networks_Type } from "../../networks";
import networks from "networks/config";

const Certification = (props) => {
  // Functions from props
  const { onGetTransactionContent } = props.R_Functions;

  // Stats frop props
  const { ContentSuccess, ContentLoading } = props.RC_Content;
  const params = props.router.params;

  // Local States
  const { TransactionData } = store.getState().Transaction;
  /*  const [params, setParams] = useState(useParams()); */
  const [hash, setHash] = useState("");
  const [network, setNetwork] = useState("");
  const [netname, setNetname] = useState("");
  //const [slogan, setSlogan] = useState("");
  const [blockexplorer, setBlockexplorer] = useState("");
  const [blockexplorerlogo, setBlockexplorerlogo] = useState(null);
  const [viewDetails, setViewDetails] = useState(false);

  // UseEffects
  useEffect(() => {
    if (params && params.hash && params.network) {
      setHash(params.hash);
      setNetwork(params.network);
      setNetname(networks(params.network).name);
      //setSlogan(networks(params.network).slogan);
      setBlockexplorer(networks(params.network).blockexplorer);
      setBlockexplorerlogo(networks(params.network).blockexplorerlogo);
      /* onGetTransactionContent(params.network, params.hash); */
    }
  }, [params]);

  // Toggles
  const toggleViewDetails = () => {
    setViewDetails(!viewDetails);
  };

  // RENDER
  return (
    <React.Fragment>
      {viewDetails && viewDetails ? (
        <section id="certificationdetails">
          <Row>
            <Col className="d-flex justify-content-center">
              <span id="certificado" className="titulo-main">
                <span>{props.t("DETAILS")}</span>
              </span>
            </Col>
          </Row>

          <Row className="mt-3 mb-4 network-logos d-flex justify-content-center">
            {Object.entries(list_Networks).map((w, j) => (
              <React.Fragment key={j}>
                {list_Networks_Strings(w[1]).includes("MainNet") && (
                  <Col lg="4" md="6" sm="12" className="d-flex justify-content-center mb-4">
                    <img
                      className={`img img-fluid  ${
                        networks(params.network).blockchain === networks(list_Networks_Type(w[1])).blockchain
                          ? "img-heighlight"
                          : "soft"
                      }`}
                      style={{ maxHeight: "60px" }}
                      src={networks(list_Networks_Type(w[1])).emblem}
                      alt={list_Networks_Type(w[1])}
                      title=""
                    />
                  </Col>
                )}
              </React.Fragment>
            ))}
          </Row>

          <Row className="mt-5">
            <Col lg="3">
              <b>{props.t("Transaction-hash")}:</b>
            </Col>
            <Col lg="9">
              <Label style={{ wordBreak: "break-word" }}>{TransactionData.hash}</Label>
            </Col>
          </Row>
          <Row className="mt-3">
            <Col lg="3" style={{ wordBreak: "break-word" }}>
              <b>{props.t("Network")}:</b>
            </Col>
            <Col lg="9">
              <Label style={{ wordBreak: "break-word" }}>{netname}</Label>
            </Col>
          </Row>
          <Row>
            <Col lg="3" style={{ wordBreak: "break-word" }}>
              <b>{props.t("Block-number")}:</b>
            </Col>
            <Col lg="9">
              <Label style={{ wordBreak: "break-word" }}>{TransactionData.blockNumber}</Label>
            </Col>
          </Row>
          <Row>
            <Col lg="3" style={{ wordBreak: "break-word" }}>
              <b>{props.t("Confirmations")}:</b>
            </Col>
            <Col lg="9">
              <Label style={{ wordBreak: "break-word" }}>{TransactionData.confirmations}</Label>
            </Col>
          </Row>
          <Row>
            <Col lg="3" style={{ wordBreak: "break-word" }}>
              <b>{props.t("From")}:</b>
            </Col>
            <Col lg="9">
              <Label style={{ wordBreak: "break-word" }}>{TransactionData.from}</Label>
            </Col>
          </Row>
          <Row className="mb-4">
            <Col lg="3" style={{ wordBreak: "break-word" }}>
              <b>{props.t("To")}:</b>
            </Col>
            <Col lg="8">
              <Label style={{ wordBreak: "break-word" }}>{TransactionData.to}</Label>
            </Col>
          </Row>
          <Row className="px-3">
            <Col lg="12" className="d-flex justify-content-end">
              <i className="bx bx-chevron-left-circle icono-medio icono-click" onClick={toggleViewDetails}></i>
            </Col>
          </Row>
        </section>
      ) : (
        <section id="certification">
          <Row>
            <Col className="d-flex justify-content-center align-items-center titulo-main">
              <i className="bx bx-check-circle verde icono-grande"></i>
              <span className="text36">{props.t("PASSED")}</span>
            </Col>
          </Row>
          <Row className="mt-3 mb-3 network-logos d-flex justify-content-center">
            {Object.entries(list_Networks).map((w, j) => (
              <React.Fragment key={j}>
                {list_Networks_Strings(w[1]).includes("MainNet") && (
                  <Col lg="4" md="6" sm="12" className="d-flex justify-content-center mb-4">
                    <img
                      className={`img img-fluid  ${
                        networks(params.network).blockchain === networks(list_Networks_Type(w[1])).blockchain
                          ? "img-heighlight"
                          : "soft"
                      }`}
                      style={{ maxHeight: "60px" }}
                      src={networks(list_Networks_Type(w[1])).emblem}
                      alt={list_Networks_Type(w[1])}
                      title=""
                    />
                  </Col>
                )}
              </React.Fragment>
            ))}
          </Row>
          <Row className="pt-3">
            <Col lg="12">
              <p className="textcentered text14">
                <strong>
                  {props.t("Secure-registered")}:
                  <br />
                  {`${netname}`.toUpperCase()}
                </strong>
              </p>
              {/* <p className="textcentered text14">{props.t(slogan)}</p> */}
            </Col>
          </Row>
          <Row className="pt-3">
            <Col lg="3"></Col>
            <Col lg="6">
              <Row className="bg-success btn-rounded">
                <Col lg="10" className="d-flex justify-content-center p-2">
                  <span className="text-white text16">{props.t("hash-verified")}</span>
                </Col>
                <Col lg="2" className="d-flex justify-content-center">
                  <i className="bx bx-check-shield text-white icono-medio"></i>
                </Col>
              </Row>
            </Col>
            <Col lg="3"></Col>
          </Row>
          <Row className="mt-5">
            <Col lg="9">
              <p className="text14">
                {props.t("transaction-id")}:
                <Label className="text-success" style={{ wordBreak: "break-word" }}>
                  {hash}
                </Label>
              </p>
            </Col>

            <Col lg="3" className="d-flex justify-content-around bottom-nav">
              <a href={`${blockexplorer}/tx/${hash}`} target="_blank" rel="noopener noreferrer">
                <img
                  className="img img-fluid"
                  style={{ maxWidth: "40px", maxHeight: "40px" }}
                  src={blockexplorerlogo}
                  alt=""
                  title=""
                />
              </a>

              {isMobile ? (
                <>
                  <PDFDownloadLink document={<PdfDocument network={network} />} fileName={TransactionData.hash}>
                    {({ blob, url, loading, error }) =>
                      loading ? "Loading" : <i className="bx bx-file icono-medio"></i>
                    }
                  </PDFDownloadLink>
                </>
              ) : (
                <Link to={`/pdf/${network}/${hash}`} target="_blank" rel="noopener noreferrer">
                  <i className="bx bx-file icono-medio"></i>
                </Link>
              )}

              <i className="bx bx-chevron-right-circle icono-medio icono-click" onClick={toggleViewDetails}></i>
            </Col>
          </Row>
          <Row className="mt-3">
            <Col lg="1"></Col>
            <Col lg="10">
              {/* {ContentLoading && <Loader2 />} */}
              {networks(props.network).contracts.includes(props.contract) && (
                <>
                  <Link to={`/verify/${network}/${hash}`} style={{ textDecoration: "none" }}>
                    <Alert color="primary" className="text-center">
                      <p className="text-dark mt-3">
                        <b>{props.t("iCommunityEvidence")}</b>
                      </p>
                      <p className="text-primary">
                        <u>{props.t("canVerifyItHere")}</u>
                      </p>
                    </Alert>
                  </Link>
                </>
              )}
            </Col>
            <Col lg="1"></Col>
          </Row>
        </section>
      )}
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  return {
    RC_Content: state.Content,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onGetTransactionContent: (network, hash, navigate) => dispatch(getTransactionContent(network, hash, navigate)),
  },
});

export default withRouter(connect(mapStatetoProps, mapDispatchToProps)(withTranslation()(Certification)));
