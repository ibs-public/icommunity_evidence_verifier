import React from "react";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Row, Col } from "reactstrap";

// Images
import icommunitySeil from "assets/images/trademark/identitySeal.png";

// i18n
import { withTranslation } from "react-i18next";

const IcommunitySignatureModal = (props) => {
  const { isOpen, toggle } = props;
  // RENDER
  return (
    <Modal isOpen={isOpen} role="dialog" autoFocus={true} centered={true} size="md" tabIndex="-1" toggle={toggle}>
      <div className="modal-content">
        <ModalHeader toggle={toggle} style={{ backgroundColor: "#ECEFF6" }}>
          {props.t("CertyfiedByIcommunity")}
        </ModalHeader>
        <ModalBody className="px-4">
          <Row>
            <Col className="d-flex justify-content-center">
              <img width={"125px"} height={"125px"} src={icommunitySeil} alt="Certified by iCommunity" />
            </Col>
          </Row>
          <Row className="mt-3">
            <Col>
              <p style={{ textAlign: "justify" }}>{props.t("SignatureCertyfiedByIcommunityText")}</p>
            </Col>
          </Row>
        </ModalBody>
      </div>
    </Modal>
  );
};

export default withTranslation()(IcommunitySignatureModal);
