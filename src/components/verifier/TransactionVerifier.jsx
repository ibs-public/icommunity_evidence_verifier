import React, { useState, useEffect } from "react";
import { Card, CardHeader, CardBody, Row, Col, Label, Button, FormGroup, Input, Alert } from "reactstrap";
import * as config from "config";

// Dropzone
import Dropzone from "react-dropzone";

// Routes
import withRouter from "helpers/router/withRouter";

// Actions
import { verifyTransaction, getTransactionContent } from "store/actions";

//Loader
import Loader2 from "../commons/Loader2";

// Networks
import networks from "networks/config";
import { list_Networks, list_Networks_Strings, list_Networks_Type } from "networks";

// Redux
import { connect } from "react-redux";

// Helpers
import formatBytesSizes from "helpers/utils/formatBytesSizes";
import isStringValidJSON from "helpers/utils/isStringValidJSON";

// Notifications
import Swal from "sweetalert2";

// Components
import IcommunitySignatureModal from "./IcommunitySignature-Modal";

// Images
import icommunitySeil from "assets/images/trademark/identitySeal.png";

//i18n
import { withTranslation } from "react-i18next";

const VerifyTransaction = (props) => {
  // Functions from props
  const { onVerifyTransaction, onGetTransactionContent } = props.R_Functions;

  // Stats frop props
  const { ContentData } = props.RC_Content;
  const { VerifierSuccess, VerifierError, VerifierLoading } = props.RC_Verifier;
  const { network, hash } = props.router.params;

  // Local States
  const [attachedTextReceived, setAttachedTextReceived] = useState("");
  const [contentTextExist, setContentTextExist] = useState(false);

  const [contentFilesCounter, setContentFilesCounter] = useState(0);
  const [checksumProvidedByIcommunity, setChecksumProvidedByIcommunity] = useState(true);
  const maxAttachedTextLength = 1024;
  const [attachedTextLength, setAttachedTextLength] = useState(maxAttachedTextLength);

  const [ffReceived, setFfReceived] = useState([]);
  const [ffUploaded, setFfUploaded] = useState([]);
  const [refreshTable, setRefreshTable] = useState(false);
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const [signatures, setSignatures] = useState([]);
  const [showIcommunitySignatureModal, setShowIcommunitySignatureModal] = useState(false);

  const [codification, setCodification] = useState(null);
  // UseEffects
  useEffect(() => {
    onGetTransactionContent(network, hash);
  }, [props.router.params]);

  useEffect(() => {
    if (ContentData && !isEmptyObject(ContentData)) {
      let checksum = JSON.parse(ContentData.contentChecksum);
      let cont = 0;
      for (let i = 0; i < checksum.length; i++) {
        if (checksum[i].type === "text") {
          setContentTextExist(true);
          setCodification(checksum[i].algorithm);
        }
        if (checksum[i].type === "file") {
          if (
            checksum[i].checksum.toUpperCase() !==
            "z4PhNX7vuL3xVChQ1m2AB9Yg5AULVxXcg/SpIdNs6c5H0NE8XYXysP+DGNKHfuwvY7kxvUdBeoGlODJ6+SfaPg==".toUpperCase()
          ) {
            cont++;
          }
          if (!(checksum[i].algorithm && checksum[i].algorithm !== "")) {
            setChecksumProvidedByIcommunity(false);
          } else {
            setCodification(checksum[i].algorithm);
          }
        }
      }
      setContentFilesCounter(cont);
      // TRABAJO CON LA FIRMA
      let userIdentity = ContentData.userIdentity;
      let temp = [];
      if (isStringValidJSON(userIdentity)) {
        userIdentity = JSON.parse(userIdentity);
        for (let i = 0; i < userIdentity.length; i++) {
          if (
            !userIdentity[i].hasOwnProperty("made_by_ibs") ||
            (userIdentity[i].hasOwnProperty("made_by_ibs") && userIdentity[i].made_by_ibs === true)
          ) {
            // INTERNAL
            if (userIdentity[i].provider && userIdentity[i].provider === "ipfs") {
              let signature = {};
              signature.name = userIdentity[i].name;
              signature.address = `${config.env.REACT_APP_IPFS_URL}/${userIdentity[i].id}`;
              temp.push(signature);
            } else {
              let signature = {};
              if (signature.name) {
                signature.name = userIdentity[i].name;
                signature.address = userIdentity[i].id;
              } else {
                let identity = JSON.stringify(userIdentity[i]);
                signature.name = identity;
                signature.address = identity;
              }
              temp.push(signature);
            }
          } else {
            // EXTERNAL
            let signature = {};
            signature.name = userIdentity[i].name;
            signature.address = userIdentity[i].id;
            signature.provider = userIdentity[i].provider;
            signature.external = true;
            temp.push(signature);
          }
        }
      } else {
        let signature = {};
        signature.name = userIdentity;
        signature.address = userIdentity;
        temp.push(signature);
      }
      setSignatures(temp);
    } else {
      setContentTextExist(false);
      setContentFilesCounter(0);
    }
  }, [ContentData]);

  useEffect(() => {
    if (contentFilesCounter > 0) {
      let tempArray01 = [];
      let tempArray02 = [];
      for (let i = 0; i < contentFilesCounter; i++) {
        let file = [];
        tempArray01.push(file);
        tempArray02.push(false);
      }
      setFfReceived(tempArray01);
      setFfUploaded(tempArray02);
    }
  }, [contentFilesCounter]);

  useEffect(() => {
    toggleSendButton();
  }, [refreshTable, attachedTextLength]);

  useEffect(() => {
    if (VerifierSuccess === "MsgOk[Successful Evidence Verification]") {
      Swal.fire({
        title: props.t("Verified!"),
        text: props.t(VerifierSuccess),
        icon: "success",
        confirmButtonText: props.t("Accept"),
      });
    } else if (VerifierSuccess === "MsgOk[Failed Evidence Verification]") {
      Swal.fire({
        title: props.t("NotVerified!"),
        text: props.t(VerifierSuccess),
        icon: "error",
        confirmButtonText: props.t("Accept"),
      });
    }
  }, [VerifierSuccess]);

  const handleAttachedTextReceivedChange = (event) => {
    if (maxAttachedTextLength - event.target.value.length >= 0) {
      setAttachedTextReceived(event.target.value);
      setAttachedTextLength(maxAttachedTextLength - event.target.value.length);
    } else {
      setAttachedTextReceived(attachedTextReceived);
    }
  };

  const handleDeleteFile = (index) => {
    let tempArray01 = ffReceived;
    tempArray01[index] = [];
    setFfReceived(tempArray01);

    let tempArray02 = ffUploaded;
    tempArray02[index] = false;
    setFfUploaded(tempArray02);
    setRefreshTable(!refreshTable);
  };

  const handleAcceptedFile = (files, index) => {
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytesSizes(file.size),
      })
    );
    let tempArray01 = ffReceived;
    tempArray01[index] = files;
    setFfReceived(tempArray01);
    let tempArray02 = ffUploaded;
    tempArray02[index] = true;
    setFfUploaded(tempArray02);
    setRefreshTable(!refreshTable);
  };

  const formValidator = () => {
    //---------->>>>>>PENDIENTE: REALIZAR LAS COMPROBACIONES NECESARTIAS ANTES DE ENVIAR<<<<<<------------
    return true;
  };
  const handleVerify = () => {
    if (formValidator()) {
      let files = [];
      for (let i = 0; i < ffReceived.length; i++) {
        let file = {};
        file.name = ffReceived[i][0].name;
        file.file = ffReceived[i][0];
        files.push(file);
      }
      let data = {
        text: attachedTextReceived,
        files: files,
      };
      onVerifyTransaction(data, ContentData.contentChecksum);
    }
  };

  // TOGGLE
  const toggleSendButton = () => {
    let allFilesUploaded = true;
    for (let i = 0; i < ffUploaded.length; i++) {
      if (ffUploaded[i] === false) {
        allFilesUploaded = false;
      }
    }
    if (contentTextExist === true && contentFilesCounter > 0) {
      if (attachedTextLength < maxAttachedTextLength && allFilesUploaded === true) {
        setButtonDisabled(false);
      } else {
        setButtonDisabled(true);
      }
    } else if (contentTextExist === true) {
      if (attachedTextLength < maxAttachedTextLength) {
        setButtonDisabled(false);
      } else {
        setButtonDisabled(true);
      }
    } else if (contentFilesCounter > 0) {
      if (allFilesUploaded === true) {
        setButtonDisabled(false);
      } else {
        setButtonDisabled(true);
      }
    } else {
      setButtonDisabled(true);
    }
  };

  const toggleShowIcommunitySignatureModal = () => {
    setShowIcommunitySignatureModal(!showIcommunitySignatureModal);
  };

  // Helpers
  const isEmptyObject = (obj) => {
    return Object.keys(obj).length === 0;
  };

  function setImgClasses(net) {
    if (networks(network).blockchain === net) {
      return "img img-fluid img-heighlight";
    } else {
      return "img img-fluid soft";
    }
  }
  
  const FileTable = () => {
    return (
      <Row className="d-flex justify-content-center">
        {ffReceived.map((f, i) => {
          return (
            <Col lg={ffReceived.length < 3 ? 12 / ffReceived.length : 3} key={i}>
              {ffUploaded[i] ? (
                <>
                  <div className="dropzone-previews">
                    {ffReceived[i].map((ff, ii) => {
                      return (
                        <Card
                          className="mt-1 mb-0 d-flex justify-content-center align-items-center"
                          style={{ minHeight: "168px" }}
                          key={ii + "-file"}
                        >
                          <Row className="p-4">
                            <Col lg="12">
                              <u>
                                <b>{ff.name}</b>
                              </u>
                              <p className="mb-0 text-muted">{ff.formattedSize}</p>
                              <Button
                                color="link"
                                size="sm"
                                id="tt-del"
                                onClick={() => handleDeleteFile(i)}
                                style={{ position: "absolute", right: "1px", top: "1px" }}
                              >
                                <i className="text-muted bx bx-trash" />
                              </Button>
                            </Col>
                          </Row>
                        </Card>
                      );
                    })}
                  </div>
                </>
              ) : (
                <>
                  <Card className="mt-1 mb-0" key={i + "-file"}>
                    <div className="p-2">
                      <Row>
                        <Col lg="12">
                          <Dropzone onDrop={(acceptedFiles) => handleAcceptedFile(acceptedFiles, i)}>
                            {({ getRootProps, getInputProps }) => (
                              <div className="rounded" style={{ minHeight: "150px", border: "2px dashed grey" }}>
                                <div
                                  /* className="dz-message needsclick" */
                                  {...getRootProps()}
                                >
                                  <input {...getInputProps()} />
                                  <div className="d-flex justify-content-center m-2">
                                    <i
                                      className="text-muted bx bxs-cloud-upload mx-2"
                                      style={{ fontSize: "2.5rem" }}
                                    ></i>
                                  </div>

                                  <h6 className="text-muted text-center m-2">{props.t("DropOrClick")}.</h6>
                                </div>
                              </div>
                            )}
                          </Dropzone>
                        </Col>
                      </Row>
                      {}
                    </div>
                  </Card>
                </>
              )}
            </Col>
          );
        })}
      </Row>
    );
  };

  // RENDER
  return (
    <React.Fragment>
      <section id="verifytransaction">
        <Row>
          <Col className="d-flex justify-content-center">
            <span id="certificado" className="titulo-main">
              <span>{props.t("Verifier")}</span>
            </span>
          </Col>
        </Row>
        <Row className="mt-3 mb-4 network-logos d-flex justify-content-center">
          {Object.entries(list_Networks).map((w, j) => (
            <React.Fragment key={j}>
              {list_Networks_Strings(w[1]).includes("MainNet") && (
                <Col lg="4" md="6" sm="12" className="d-flex justify-content-center mb-4">
                  <img
                    className={`img img-fluid  ${
                      networks(network).blockchain === networks(list_Networks_Type(w[1])).blockchain
                        ? "img-heighlight"
                        : "soft"
                    }`}
                    style={{ maxHeight: "60px" }}
                    src={networks(list_Networks_Type(w[1])).emblem}
                    alt={list_Networks_Type(w[1])}
                    title=""
                  />
                </Col>
              )}
            </React.Fragment>
          ))}
        </Row>
        {VerifierError ? (
          <>
            {VerifierError === "MsgErr[Transaction not found]" ? (
              <Row>
                <Col>
                  <Alert color="danger" className="mt-5 p-4">
                    <h5 className="text-center">{props.t("Verifier_Transaction_not_found")}</h5>
                    <br />
                    <h5 className="text-center">{props.t("Please_Check_Data")}</h5>
                  </Alert>
                </Col>
              </Row>
            ) : (
              <Alert color="danger" className="text-center" style={{ marginTop: "13px" }}>
                <p className="mt-2">{props.t("MsgErr[Generical error]")}</p>
                <a href={"mailto:development@icommunity.com"}>development@icommunity.com</a>
                <p className="mt-4">{props.t("SorryForTheInconvenience")}</p>
              </Alert>
            )}
          </>
        ) : (
          <>
            {ContentData && !isEmptyObject(ContentData) && (
              <>
                <Row className="px-4">
                  <Col lg="12">
                    <Card className="border-0 mt-4 mx-3 bg-transparent">
                      <CardHeader className="border-0 pb-0 bg-transparent">
                        <p className="text-dark text-uppercase">
                          <b>{props.t("TransactionContent")}</b>
                        </p>
                      </CardHeader>
                      <CardBody className="mb-4">
                        <Row>
                          <Col lg="12">
                            <>
                              <Row className="mb-3 pb-2 border-bottom">
                                <Col lg="2">
                                  <b>{props.t("Title")}:</b>
                                </Col>
                                <Col lg="10">
                                  {ContentData.title}
                                  {parseInt(ContentData.version) > 1 && ` (${ContentData.version})`}
                                </Col>
                              </Row>
                              <Row className="mb-3 pb-2 border-bottom">
                                <Col lg="2">
                                  <b>{props.t("SignedBy")}:</b>
                                </Col>
                                <Col lg="10">
                                  {signatures.map((element, index) => {
                                    if (element.external) {
                                      return (
                                        <Row key={index}>
                                          <Col>{`${element.name} (${element.provider}: ${element.address})`}</Col>
                                        </Row>
                                      );
                                    } else {
                                      return (
                                        <Row key={index}>
                                          <Col>
                                            {element.address.startsWith("http") ? (
                                              <>
                                                <img
                                                  width={"30px"}
                                                  height={"30px"}
                                                  style={{ cursor: "help" }}
                                                  src={icommunitySeil}
                                                  alt="Certified by iCommunity"
                                                  title={props.t("iCommunitySignature")}
                                                  onClick={toggleShowIcommunitySignatureModal}
                                                  className="my-1"
                                                />
                                                <a
                                                  href={`${element.address}`}
                                                  target="_blank"
                                                  rel="noreferrer"
                                                  className="mx-2"
                                                >
                                                  {element.name}
                                                </a>
                                              </>
                                            ) : (
                                              <>{element.name}</>
                                            )}
                                          </Col>
                                        </Row>
                                      );
                                    }
                                  })}
                                </Col>
                              </Row>

                              <Row className="mt-4 mb-3">
                                <Col lg="12" className="text-center">
                                  <b>{props.t("DataIntegrity")}:</b>
                                </Col>
                              </Row>
                              <Row className="mb-2">
                                <Col lg="12">
                                  {checksumProvidedByIcommunity ? (
                                    <Row>
                                      <Col lg="4" className="border py-2">
                                        <b>{props.t("Name")}</b>
                                      </Col>
                                      <Col lg="4" className="border py-2">
                                        <b>{props.t("Checksum")}</b>
                                      </Col>
                                      <Col lg="2" className="border py-2">
                                        <b>{props.t("Algorithm")}</b>
                                      </Col>
                                      <Col lg="2" className="border py-2">
                                        <b>{props.t("Encoding")}</b>
                                      </Col>
                                    </Row>
                                  ) : (
                                    <Row>
                                      <Col lg="5" className="border py-2">
                                        <b>{props.t("Name")}</b>
                                      </Col>
                                      <Col lg="7" className="border py-2">
                                        <b>{`${props.t("Checksum")} *`}</b>
                                      </Col>
                                    </Row>
                                  )}

                                  {JSON.parse(ContentData.contentChecksum).map((data, index) => {
                                    if (data.type === "text") {
                                      return (
                                        <Row key={index}>
                                          <Col lg="4" className="border py-2">
                                            {props.t("Text")}
                                          </Col>
                                          <Col lg="4" className="border py-2">
                                            {data.checksum}
                                          </Col>
                                          <Col lg="2" className="border py-2">
                                            {props.t(data.algorithm ? data.algorithm : "")}
                                          </Col>
                                          <Col lg="2" className="border py-2">
                                            {props.t(data.sanitizer ? data.sanitizer : "")}
                                          </Col>
                                        </Row>
                                      );
                                    }
                                    if (data.type === "file") {
                                      return (
                                        <Row key={index}>
                                          {checksumProvidedByIcommunity ? (
                                            <>
                                              <Col lg="4" className="border py-2">
                                                {data.name}
                                              </Col>
                                              <Col lg="4" className="border py-2">
                                                {data.checksum.toUpperCase() ===
                                                "z4PhNX7vuL3xVChQ1m2AB9Yg5AULVxXcg/SpIdNs6c5H0NE8XYXysP+DGNKHfuwvY7kxvUdBeoGlODJ6+SfaPg==".toUpperCase()
                                                  ? props.t("EmptyFile")
                                                  : data.checksum}
                                              </Col>
                                              <Col lg="2" className="border py-2">
                                                {data.checksum.toUpperCase() ===
                                                "z4PhNX7vuL3xVChQ1m2AB9Yg5AULVxXcg/SpIdNs6c5H0NE8XYXysP+DGNKHfuwvY7kxvUdBeoGlODJ6+SfaPg==".toUpperCase()
                                                  ? ""
                                                  : props.t(data.algorithm ? data.algorithm : "")}
                                              </Col>
                                              <Col lg="2" className="border py-2">
                                                {data.checksum.toUpperCase() ===
                                                "z4PhNX7vuL3xVChQ1m2AB9Yg5AULVxXcg/SpIdNs6c5H0NE8XYXysP+DGNKHfuwvY7kxvUdBeoGlODJ6+SfaPg==".toUpperCase()
                                                  ? ""
                                                  : props.t(data.sanitizer ? data.sanitizer : "")}
                                              </Col>
                                            </>
                                          ) : (
                                            <>
                                              <Col lg="5" className="border py-2">
                                                {data.name}
                                              </Col>
                                              <Col lg="7" className="border py-2">
                                                {data.checksum}
                                              </Col>
                                            </>
                                          )}
                                        </Row>
                                      );
                                    }
                                  })}
                                </Col>
                              </Row>
                              {!checksumProvidedByIcommunity && (
                                <>
                                  <Label className="mx-1 font-weight-bold">*</Label>
                                  <Label className="font-size-8">
                                    {props.t("DisclaimerChecksumNotProvidedByIcommunity")}
                                  </Label>
                                </>
                              )}
                            </>
                          </Col>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
                <Row className="px-4">
                  <Col lg="12">
                    <Card className="border-0 mt-4 mx-3 bg-transparent">
                      {checksumProvidedByIcommunity && (
                        <CardHeader className="border-0 pb-0 bg-transparent">
                          <p className="text-dark text-uppercase">
                            <b>{props.t("VerifierData")}</b>
                          </p>
                          <p className="font-size-8">
                            <b>{props.t("Attention")}: </b>
                            {props.t("verifier_instructions")}
                          </p>
                        </CardHeader>
                      )}
                      <CardBody>
                        {contentTextExist && contentTextExist === true && (
                          <Row className="container-fluid">
                            <Col lg="12">
                              <Card className="border-0 bg-transparent">
                                <CardBody>
                                  <Row>
                                    <Col lg="12">
                                      <FormGroup className="text-dark">
                                        <Label>{props.t("Text")}: </Label>
                                        <Input
                                          type="textarea"
                                          style={{
                                            minHeight: "150px",
                                          }}
                                          placeholder={props.t("p_value")}
                                          value={attachedTextReceived}
                                          onChange={handleAttachedTextReceivedChange}
                                        />
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col lg="12" className="text-end">
                                      <Label className="font-size-9">
                                        {attachedTextLength}/{maxAttachedTextLength}
                                      </Label>
                                    </Col>
                                  </Row>
                                </CardBody>
                              </Card>
                            </Col>
                          </Row>
                        )}
                        {contentFilesCounter > 0 && checksumProvidedByIcommunity && (
                          <Row className="container-fluid">
                            <Col lg="12">
                              <Card className="border-0 bg-transparent">
                                <CardBody>
                                  <label>{props.t("Files")}:</label>
                                  <FileTable />
                                </CardBody>
                              </Card>
                            </Col>
                          </Row>
                        )}
                        {(contentTextExist === true || (contentFilesCounter > 0 && checksumProvidedByIcommunity)) && (
                          <>
                            <Row className="mt-5">
                              <Col className="d-flex justify-content-center">
                                {VerifierLoading ? (
                                  <Button
                                    color="light"
                                    className="btn btn-rounded"
                                    style={{
                                      width: "30%",
                                      minWidth: "200px",
                                    }}
                                    size="md"
                                    disabled
                                  >
                                    <Loader2 />
                                  </Button>
                                ) : (
                                  <Button
                                    color="primary"
                                    className="btn btn-rounded"
                                    style={{
                                      width: "30%",
                                      minWidth: "200px",
                                    }}
                                    size="md"
                                    onClick={handleVerify}
                                    disabled={buttonDisabled}
                                  >
                                    {props.t("Check")}
                                  </Button>
                                )}
                              </Col>
                            </Row>
                          </>
                        )}
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </>
            )}
          </>
        )}
      </section>
      {showIcommunitySignatureModal && (
        <IcommunitySignatureModal isOpen={showIcommunitySignatureModal} toggle={toggleShowIcommunitySignatureModal} />
      )}
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  return {
    RC_Content: state.Content,
    RC_Verifier: state.Verifier,
  };
};

const mapDispatchToProps = (dispatch) => ({
  R_Functions: {
    onVerifyTransaction: (data, integrity) => dispatch(verifyTransaction(data, integrity)),
    onGetTransactionContent: (network, hash) => dispatch(getTransactionContent(network, hash)),
  },
});

export default withRouter(connect(mapStatetoProps, mapDispatchToProps)(withTranslation()(VerifyTransaction)));
