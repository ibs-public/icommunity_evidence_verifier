import React from "react";
import { Text, View, Image, StyleSheet } from "@react-pdf/renderer";

import PdfQR from "./pdf_qr";

//images
import handshake from "assets/images/trademark/handshake.png";
import ministerioLogo from "assets/images/trademark/ministerioLogo.jpg";

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: "row",
    marginTop: 3,
    padding: 10,
  },
  footContainer: {
    flexDirection: "row",
  },
  foot2Container: {
    height: 80,
    width: 80,
    marginLeft: 10,
  },
  foot3Container: {
    height: 80,
    width: 200,
    marginLeft: 200,
  },
  sealContainer: {
    flexDirection: "row",
    fontSize: 9,
    marginTop: 8,
    marginLeft: 2,
  },
  imageContainer: {
    flexDirection: "row",
    marginLeft: 4,
  },
  text: {
    width: "100%",
    fontSize: 10,
    textAlign: "justify",
  },
  handshake: {
    width: 35,
    height: 35,
  },
  ministerioLogo: {
    width: 140,
    height: 35,
  },
});

const PdfFooter = (props) => (
  <>
    <View style={styles.titleContainer}>
      <Text style={styles.text}>
        iCommunity Labs, certifies the registry as a trusted service provider. Officially registered in the Ministry of
        Economic Affairs and Digital Transformation of Spain, in accordance with the EU Regulation 910/2014. Providing
        legal security with effective electronic evidence.
      </Text>
    </View>
    <View style={styles.footContainer}>
      <View style={styles.foot2Container}>
        <PdfQR hash={props.hash} network={props.network} />
      </View>
      <View style={styles.foot3Container}>
        <View style={styles.sealContainer}>
          <Text style={styles.text}>Electronic Trust Service Provider (ETSP)</Text>
        </View>
        <View style={styles.imageContainer}>
          <Image style={styles.handshake} src={handshake} />
          <Image style={styles.ministerioLogo} src={ministerioLogo} />
        </View>
      </View>
    </View>
  </>
);

export default PdfFooter;
