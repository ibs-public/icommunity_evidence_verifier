import React from "react";

import {
  Image,
  StyleSheet,
} from "@react-pdf/renderer";
import QRCode from "qrcode";

// Create styles
const styles = StyleSheet.create({  
  QRImage: {
    width: 75,
    height: 75,
  },
});

// Create Document Component
const PDFQR = (props) => {
  const qrImage = QRCode.toDataURL(`${window.location.origin}/check/${props.network}/${props.hash}`, { type: "png" });
  return (
      <Image src={qrImage} style={styles.QRImage} />
  );
};

//export default withTranslation(PDFDocument)
export default PDFQR;
