import React from "react";
import { View, Text, StyleSheet } from "@react-pdf/renderer";

const styles = StyleSheet.create({
  container: {
    borderColor: "black",
    borderWidth: 1,
    padding: 15,
  },

  row: {
    flexDirection: "row",
  },
  header: {
    width: "15%",
    textAlign: "left",
    paddingLeft: 2,
    fontSize: 10,
  },
  description: {
    width: "85%",
    textAlign: "left",
    paddingLeft: 2,
    fontSize: 10,
  },
});

const InvoiceItemsTable = ({ TransactionData, network }) => {
  const truncateString = (str) => {
    const long = 66;
    let str2 = str.slice(0, long - 1);
    for (var i = long; i < str.length; i = i + long) {
      str2 = str2 + "\n" + str.slice(i, i + long - 1);
    }
    return str2;
  };
  const data = truncateString(TransactionData.data);

  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <Text style={styles.header}>Hash:</Text>
        <Text style={styles.description}>{TransactionData.hash}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.header}>Network:</Text>
        <Text style={styles.description}>{network}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.header}>Block Hash:</Text>
        <Text style={styles.description}>{TransactionData.blockHash}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.header}>Block Nº:</Text>
        <Text style={styles.description}>{TransactionData.blockNumber}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.header}>Chain Id:</Text>
        <Text style={styles.description}>{TransactionData.chainId}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.header}>From:</Text>
        <Text style={styles.description}>{TransactionData.from}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.header}>To:</Text>
        <Text style={styles.description}>{TransactionData.to}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.header}>R:</Text>
        <Text style={styles.description}>{TransactionData.r}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.header}>S:</Text>
        <Text style={styles.description}>{TransactionData.s}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.header}>V:</Text>
        <Text style={styles.description}>{TransactionData.v}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.header}>Data:</Text>
        <Text style={styles.description}>{data}</Text>
      </View>
    </View>
  );
};

export default InvoiceItemsTable;
