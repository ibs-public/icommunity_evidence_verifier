import React from "react";
import { Page, Document, StyleSheet } from "@react-pdf/renderer";

//store
import store from "store";

//Pdf components
import PdfHeader from "./pdf_header";
import PdfTableItems from "./pdf_table_items";
import PdfFooter from "./pdf_footer";

// Create styles
const styles = StyleSheet.create({
  page: {
    fontFamily: "Helvetica",
    fontSize: 11,
    paddingTop: 30,
    paddingLeft: 60,
    paddingRight: 60,
    lineHeight: 1.5,
    flexDirection: "column",
  },
  logoicommunity: {
    width: 200,
    height: 42,
    marginLeft: 20,
  },
  ethereumLogo: {
    width: 200,
    height: 42,
    marginLeft: 40,
  },
});

// Create Document Component
const PDFDocument = (props) => {
  const { TransactionData } = store.getState().Transaction;
  const { network } = props;
  return (
    <Document title={TransactionData.hash} author="iCommunity labs">
      <Page size="A4" style={styles.page}>
        <PdfHeader network={network} />
        <PdfTableItems TransactionData={TransactionData} network={network} />
        <PdfFooter hash={TransactionData.hash} network={network} />
      </Page>
    </Document>
  );
};

export default PDFDocument;
