import React, { useEffect, useState } from "react";
import { Text, View, Image, StyleSheet } from "@react-pdf/renderer";

// Networks
import networks from "networks/config";

//images
import logoicommunity from "assets/images/trademark/icommunity@2x.png";

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: "row",
    marginTop: 12,
    display: "flex",
    justifyContent: "center",
  },
  titleContainer2: {
    flexDirection: "row",
    marginTop: 25,
    marginLeft: 15,
  },
  reportTitle: {
    color: "#386df0",
    letterSpacing: 4,
    fontSize: 20,
    textAlign: "center",
    textTransform: "uppercase",
  },
  logoicommunity: {
    height: 50,
    marginRight: 65,
    padding: 7,
  },
  emblem: {
    height: 50,
    marginLeft: 65,
    padding: 7,
  },
});

const PdfHeader = (props) => {
  const { network } = props;

  const [emblem, setEmblem] = useState(null);

  useEffect(() => {
    if (network) {
      setEmblem(networks(network).emblem);
    }
  }, [network]);

  return (
    <>
      <View style={styles.titleContainer}>
        <Image style={styles.logoicommunity} src={logoicommunity} />
        <Image style={styles.emblem} src={emblem} />
      </View>
      <View style={styles.titleContainer2}>
        <Text style={styles.reportTitle}>Certificate:</Text>
      </View>
    </>
  );
};

export default PdfHeader;
