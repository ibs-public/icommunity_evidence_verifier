import React from "react";

import { Puff } from "react-loader-spinner";

const App = () => {
  return (
    <React.Fragment>
      <div className="container-fluid d-flex justify-content-center align-items-center loader">
        <div className="d-flex justify-content-center align-items-center">
          <Puff color="#386DF0" height={80} width={80} />
        </div>
        <div className="d-flex justify-content-center align-items-center">
          <Puff color="#386DF0" height={80} width={80} />
        </div>
        <div className="d-flex justify-content-center align-items-center">
          <Puff color="#386DF0" height={80} width={80} />
        </div>
      </div>
    </React.Fragment>
  );
};

export default App;
