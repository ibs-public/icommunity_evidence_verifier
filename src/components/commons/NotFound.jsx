import React from "react";
import { Container, Row, Col } from "reactstrap";

// Images
import NotFound from "assets/images/miscellany/404NotFound.gif";

const UnderConstruction = (props) => {
  // Render
  return (
    <Container className="container-fluid">
      <br />
      <Row>
        <Col className="d-flex justify-content-center">
          <img src={NotFound} alt="" />
        </Col>
      </Row>
    </Container>
  );
};

export default UnderConstruction;
