import React from "react";
import { Container, Row, Col, Label } from "reactstrap";

//package.json
const datos = require("../../../package.json");

const Footer = () => {
  return (
    <React.Fragment>
      <footer className="footer">
        <Container className="container-fluid">
          <Row>
            <Col lg={12} className="d-flex justify-content-center">
              <Label>© {new Date().getFullYear()} iCommunity Labs.</Label>
              <Label>v. {datos.version}</Label>
            </Col>
          </Row>
        </Container>
      </footer>
    </React.Fragment>
  );
};

export default Footer;
