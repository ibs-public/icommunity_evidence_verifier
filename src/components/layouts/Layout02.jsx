// React
import React from "react";

// i18n
import { withTranslation } from "react-i18next";

// COMMON COMPONENTS
import Footer from "../commons/Footer";

//images
import logoicommunity from "assets/images/trademark/icommunity@2x.png";
import handshake from "assets/images/trademark/handshake.png";
import ministerioLogo from "assets/images/trademark/ministerioLogo.jpg";

const Layout = (props) => {
  return (
    <div className="container">
      <header className="text-right d-sm-block m-sm-5">
        <a href="/">
          <img id="logoicommunity" src={logoicommunity} alt="" title="" />
        </a>
      </header>
      <main className="main">
        <div className="bocadillo_pdf">
          <div className="b_arriba_pdf">{props.children}</div>
          <div className="b_abajo">
            <p className="textjustified text12">
              <strong>{props.t("Notice")}:</strong> {props.t("iCommunity-certify")}
            </p>
            <div className="selloministerio">
              <p className="textcentered text12">Electronic Trust Service Provider (ETSP)</p>
              <div className="iconosministerio">
                <img className="handshake" src={handshake} alt="" title="" />
                <img className="ministerioLogo" src={ministerioLogo} alt="" title="" />
              </div>
            </div>
          </div>
        </div>
      </main>
      <br />
      <Footer />
    </div>
  );
};

export default withTranslation()(Layout);
