// REQUIRED LIBRARIES
import { ethers } from "ethers";
import * as config from "../config";

//images
import ethereumLogo from "assets/images/networks/ethereumLogo.png";
import polygonLogo from "assets/images/networks/polygonLogo.png";
import xdaiLogo from "assets/images/networks/xdaiLogo.png";
import bnbLogo from "assets/images/networks/bnbLogo.png";
import avalancheLogo from "assets/images/networks/avalancheLogo.png";
import fantomLogo from "assets/images/networks/fantomLogo.png";
import arbitrumLogo from "assets/images/networks/arbitrumLogo.png";

import etherscanLogo from "assets/images/networks/etherscanLogo.png";
import polygonscanLogo from "assets/images/networks/polygonscanLogo.png";
import xdaiscanLogo from "assets/images/networks/xdaiscanLogo.png";
import bnbscanLogo from "assets/images/networks/bnbscanLogo.png";
import avalanchescanLogo from "assets/images/networks/avalanchescanLogo.png";
import fantomscanLogo from "assets/images/networks/fantomscan.png";
import arbitrumscanLogo from "assets/images/networks/arbitrumscanLogo.png";

let networks = function (blockchain_network) {
  switch (blockchain_network) {
    case "mumbai":
      return {
        name: "Mumbai (Polygon TestNet)",
        provider: async () => await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_MUMBAI_RPC}`),
        slogan: "Polygon_Slogan",
        emblem: polygonLogo,
        blockexplorer: "https://mumbai.polygonscan.com",
        blockexplorerlogo: polygonscanLogo,
        blockchain: "polygon",
        contracts: ["0x7E6c038c64362e282a44d73E294f9d42030c38Af", "0x8c7216389Df85a57e307d01BEebe7f4769F88E92"],
      };

    case "matic":
      return {
        name: "Matic (Polygon MainNet)",
        provider: async () => await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_MATIC_RPC}`),
        slogan: "Polygon_Slogan",
        emblem: polygonLogo,
        blockexplorer: "https://polygonscan.com",
        blockexplorerlogo: polygonscanLogo,
        blockchain: "polygon",
        contracts: ["0x48F7e6E3415327eeA045023250714754BE33EABd", "0x8c7216389Df85a57e307d01BEebe7f4769F88E92"],
      };

    case "chiado":
      return {
        name: "Chiado (Gnosis TestNet)",
        provider: async () => await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_CHIADO_RPC}`),
        slogan: "xDai_Slogan",
        emblem: xdaiLogo,
        blockexplorer: "https://blockscout.chiadochain.net",
        blockexplorerlogo: xdaiscanLogo,
        blockchain: "xdai",
        contracts: ["0x48F7e6E3415327eeA045023250714754BE33EABd", "0xF5efa33C9235307C9a2b4C8F96E206fbcab401ff"],
      };

    case "xdai":
      return {
        name: "xDai (Gnosis MainNet)",
        provider: async () => await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_XDAI_RPC}`),
        slogan: "xDai_Slogan",
        emblem: xdaiLogo,
        blockexplorer: "https://blockscout.com/poa/xdai",
        blockexplorerlogo: xdaiscanLogo,
        blockchain: "xdai",
        contracts: ["0x48F7e6E3415327eeA045023250714754BE33EABd", "0x8c7216389Df85a57e307d01BEebe7f4769F88E92"],
      };

    case "goerli":
      return {
        name: "Goerli (Ethereum TestNet)",
        provider: async () => await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_ETHEREUM_GOERLI_TESTNET_RPC}`),
        slogan: "Ethereum_Slogan",
        emblem: ethereumLogo,
        blockexplorer: "https://goerli.etherscan.io",
        blockexplorerlogo: etherscanLogo,
        blockchain: "ethereum",
        contracts: ["0xb2DD395c95503CDff292F0f8014b718577dCE5F8", "0x5F5eB0F8D08C07998A0F3d0b270423923A6265a0"],
      };

    case "mainnet":
      return {
        name: "Ethereum  (Ethereum MainNet)",
        provider: async () => await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_ETHEREUM_MAINNET_RPC}`),
        slogan: "Ethereum_Slogan",
        emblem: ethereumLogo,
        blockexplorer: "https://etherscan.io",
        blockexplorerlogo: etherscanLogo,
        blockchain: "ethereum",
        contracts: ["0xacaC25816980CC33371208e033Cd6197f8acD0fd"],
      };

    case "bnb_testnet":
      return {
        name: "BNB Testnet (BNB TestNet)",
        provider: async () => await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_BNB_TESTNET_RPC}`),
        slogan: "BNB_Slogan",
        emblem: bnbLogo,
        blockexplorer: "https://testnet.bnbscan.com",
        blockexplorerlogo: bnbscanLogo,
        blockchain: "bnb",
        contracts: ["0x48F7e6E3415327eeA045023250714754BE33EABd"],
      };
    case "bnb_mainnet":
      return {
        name: "BNB (BNB MainNet)",
        provider: async () => await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_BNB_MAINNET_RPC}`),
        slogan: "BNB_Slogan",
        emblem: bnbLogo,
        blockexplorer: "https://bnbscan.com",
        blockexplorerlogo: bnbscanLogo,
        blockchain: "bnb",
        contracts: [],
      };

    case "avalanche_fuji_testnet":
      return {
        name: "Fuji (Avalanche TestNet)",
        provider: async () =>
          await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_AVALANCHE_FUJI_TESTNET_RPC}`),
        slogan: "Avalanche_Slogan",
        emblem: avalancheLogo,
        blockexplorer: "https://testnet.snowtrace.io",
        blockexplorerlogo: avalanchescanLogo,
        blockchain: "avalanche",
        contracts: ["0x48F7e6E3415327eeA045023250714754BE33EABd"],
      };
    case "avalanche_cchain_mainnet":
      return {
        name: "C-Chain (Avalanche MainNet)",
        provider: async () =>
          await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_AVALANCHE_CCHAIN_MAINNET_RPC}`),
        slogan: "Avalanche_Slogan",
        emblem: avalancheLogo,
        blockexplorer: "https://snowtrace.io",
        blockexplorerlogo: avalanchescanLogo,
        blockchain: "avalanche",
        contracts: ["0x48F7e6E3415327eeA045023250714754BE33EABd"],
      };

    case "fantom_testnet":
      return {
        name: "Fantom Testnet (Fantom TestNet)",
        provider: async () => await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_FANTOM_TESTNET_RPC}`),
        slogan: "Fantom_Slogan",
        emblem: fantomLogo,
        blockexplorer: "https://testnet.ftmscan.com",
        blockexplorerlogo: fantomscanLogo,
        blockchain: "fantom",
        contracts: ["0x48F7e6E3415327eeA045023250714754BE33EABd"],
      };
    case "fantom_opera_mainnet":
      return {
        name: "Opera (Fantom MainNet)",
        provider: async () =>
          await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_FANTOM_OPERA_MAINNET_RPC}`),
        slogan: "Fantom_Slogan",
        emblem: fantomLogo,
        blockexplorer: "https://ftmscan.com",
        blockexplorerlogo: fantomscanLogo,
        blockchain: "fantom",
        contracts: ["0x48F7e6E3415327eeA045023250714754BE33EABd"],
      };

    case "arbitrum_goerli_testnet":
      return {
        name: "Arbitrum Goerli (Arbitrum TestNet)",
        provider: async () =>
          await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_ARBITRUM_GOERLI_TESTNET_RPC}`),
        slogan: "Arbitrum_Slogan",
        emblem: arbitrumLogo,
        blockexplorer: "https://goerli.arbiscan.io",
        blockexplorerlogo: arbitrumscanLogo,
        blockchain: "arbitrum",
        contracts: ["0x48F7e6E3415327eeA045023250714754BE33EABd"],
      };
    case "arbitrum_one_mainnet":
      return {
        name: "One (Arbitrum MainNet)",
        provider: async () =>
          await new ethers.providers.JsonRpcProvider(`${config.env.REACT_APP_ARBITRUM_ONE_MAINNET_RPC}`),
        slogan: "Arbitrum_Slogan",
        emblem: arbitrumLogo,
        blockexplorer: "https://arbiscan.io",
        blockexplorerlogo: arbitrumscanLogo,
        blockchain: "arbitrum",
        contracts: [],
      };
  }
};

export default networks;
