const list_Networks = {
  /* ARBITRUM_GOERLI_TESTNET: "arbitrum_goerli_testnet", */
  /*  ARBITRUM_ONE_MAINNET: "arbitrum_one_mainnet", */
  AVALANCHE_FUJI_TESTNET: "avalanche_fuji_testnet",
  AVALANCHE_CCHAIN_MAINNET: "avalanche_cchain_mainnet",
  /* BNB_TESTNET: "bnb_testnet", */
  /* BNB_MAINNET: "bnb_mainnet", */
  GOERLI: "goerli",
  MAINNET: "mainnet",
  FANTOM_TESTNET: "fantom_testnet",
  FANTOM_OPERA_MAINNET: "fantom_opera_mainnet",
  CHIADO: "chiado",
  XDAI: "xdai",
  MUMBAI: "mumbai",
  MATIC: "matic",
};

const list_Networks_Strings = (state) => {
  switch (state) {
    case list_Networks.MUMBAI:
      return "Polygon Mumbai (TestNet)";
    case list_Networks.MATIC:
      return "Polygon Matic (MainNet)";
    case list_Networks.CHIADO:
      return "Gnosis Chiado (TestNet)";
    case list_Networks.XDAI:
      return "Gnosis xDai (MainNet)";
    case list_Networks.GOERLI:
      return "Ethereum Goerli (TestNet)";
    case list_Networks.MAINNET:
      return "Ethereum (MainNet)";
    case list_Networks.BNB_TESTNET:
      return "BNB (TestNet)";
    case list_Networks.BNB_MAINNET:
      return "BNB (MainNet)";
    case list_Networks.AVALANCHE_FUJI_TESTNET:
      return "Avalanche Fuji (TestNet)";
    case list_Networks.AVALANCHE_CCHAIN_MAINNET:
      return "Avalanche C-Chain (MainNet)";
    case list_Networks.FANTOM_TESTNET:
      return "Fantom (TestNet)";
    case list_Networks.FANTOM_OPERA_MAINNET:
      return "Fantom Opera (MainNet)";
    case list_Networks.ARBITRUM_GOERLI_TESTNET:
      return "Arbitrum Goerli (TestNet)";
    case list_Networks.ARBITRUM_ONE_MAINNET:
      return "Arbitrum One (MainNet)";
    default:
      return "UNKNOW";
  }
};

const list_Networks_Type = (state) => {
  switch (state) {
    case list_Networks.MUMBAI:
      return "mumbai";
    case list_Networks.MATIC:
      return "matic";
    case list_Networks.CHIADO:
      return "chiado";
    case list_Networks.XDAI:
      return "xdai";
    case list_Networks.GOERLI:
      return "goerli";
    case list_Networks.MAINNET:
      return "mainnet";
    case list_Networks.BNB_TESTNET:
      return "bnb_testnet";
    case list_Networks.BNB_MAINNET:
      return "bnb_mainnet";
    case list_Networks.AVALANCHE_FUJI_TESTNET:
      return "avalanche_fuji_testnet";
    case list_Networks.AVALANCHE_CCHAIN_MAINNET:
      return "avalanche_cchain_mainnet";
    case list_Networks.FANTOM_TESTNET:
      return "fantom_testnet";
    case list_Networks.FANTOM_OPERA_MAINNET:
      return "fantom_opera_mainnet";
    case list_Networks.ARBITRUM_GOERLI_TESTNET:
      return "arbitrum_goerli_testnet";
    case list_Networks.ARBITRUM_ONE_MAINNET:
      return "arbitrum_one_mainnet";
    default:
      return "UNKNOW";
  }
};

export { list_Networks, list_Networks_Strings, list_Networks_Type };
