import {
  // GET TRANSACTION
  GET_TRANSACTION,
  GET_TRANSACTION_SUCCESS,
  GET_TRANSACTION_ERROR,
  // TRANSACTION RESET
  RESET_TRANSACTION,
  // RESET
  RESET,
} from "./actionTypes";

const initialState = {
  TransactionError: "",
  TransactionSuccess: false,
  TransactionLoading: false,
  TransactionData: {},
};

const Transaction = (state = initialState, action) => {
  switch (action.type) {
    // GET TRANSACTION
    case GET_TRANSACTION:
      state = {
        ...state,
        TransactionLoading: true,
        TransactionSuccess: false,
      };
      break;
    case GET_TRANSACTION_SUCCESS:
      state = {
        ...state,
        TransactionData: action.payload,
        TransactionSuccess: true,
        TransactionLoading: false,
      };
      break;
    case GET_TRANSACTION_ERROR:
      state = {
        ...state,
        TransactionError: action.payload,
        TransactionLoading: false,
      };
      break;

    // TRANSACTION RESET
    case RESET_TRANSACTION:
      state = initialState;
      break;

    // RESET
    case RESET:
      state = initialState;
      break;

    default:
      state = {
        ...state,
      };
      break;
  }
  return state;
};

export default Transaction;
