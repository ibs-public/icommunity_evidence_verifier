import { takeEvery, put, call } from "redux-saga/effects";

// Profile Redux States
import {
  // GET TRANSACTION
  GET_TRANSACTION,
} from "./actionTypes";

import {
  // GET TRANSACTION
  getTransactionSuccess,
  getTransactionError,
} from "./actions";

import * as api from "api/transactions/transactions.service";

function* fetchTransaction({ params: { network, hash } }) {
  try {
    const response = yield call(api.getTransaction, network, hash);
    /* console.log("RESPONSE transactions fetchTransaction",response) */
    if (response.status===1){
      if (response === null) {
        yield put(getTransactionSuccess({ hash: "inexistente" }));
      } else {
        yield put(getTransactionSuccess(response.message));
      }
    }else{
      yield put(getTransactionError(response.message));
    }
   
  } catch (error) {
    yield put(getTransactionError(error));
    console.log(error);
  }
}

function* TransactionSaga() {
  yield takeEvery(GET_TRANSACTION, fetchTransaction);
}

export default TransactionSaga;
