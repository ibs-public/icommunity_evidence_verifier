import {
  // GET TRANSACTION
  GET_TRANSACTION,
  GET_TRANSACTION_SUCCESS,
  GET_TRANSACTION_ERROR,
} from "./actionTypes";

// GET TRANSACTION
export const getTransaction = (network, hash) => {
  return {
    type: GET_TRANSACTION,
    params: { network, hash },
  };
};

export const getTransactionSuccess = (result) => {
  return {
    type: GET_TRANSACTION_SUCCESS,
    payload: result,
  };
};

export const getTransactionError = (error) => {
  return {
    type: GET_TRANSACTION_ERROR,
    payload: error,
  };
};
