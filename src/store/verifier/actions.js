import {
  // Verify Transaction
  VERIFY_TRANSACTION,
  VERIFY_TRANSACTION_SUCCESS,
  VERIFY_TRANSACTION_ERROR,
} from "./actionTypes"

// Verify Transaction
export const verifyTransaction = (data, integrity) => {
  return {
    type: VERIFY_TRANSACTION,
    params: { data, integrity },
  }
}

export const verifyTransactionSuccess = obj => ({
  type: VERIFY_TRANSACTION_SUCCESS,
  payload: obj,
})

export const verifyTransactionError = error => ({
  type: VERIFY_TRANSACTION_ERROR,
  payload: error,
})