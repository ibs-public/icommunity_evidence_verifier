import {
  // Verify Transaction
  VERIFY_TRANSACTION,
  VERIFY_TRANSACTION_SUCCESS,
  VERIFY_TRANSACTION_ERROR,
  // Reset especifico
  RESET_VERIFIER,
  // Reset general
  RESET,
} from "./actionTypes";

const initialState = {
  VerifierError: null,
  VerifierSuccess: null,
  VerifierLoading: false,
};

const Verifier = (state = initialState, action) => {
  switch (action.type) {
    // Verify Transaction
    case VERIFY_TRANSACTION:
      state = {
        ...state,
        VerifierLoading: true,
        VerifierError: null,
        VerifierSuccess: null,
      };
      break;

    case VERIFY_TRANSACTION_SUCCESS:
      state = {
        ...state,
        VerifierSuccess: action.payload,
        VerifierError: null,
        VerifierLoading: false,
      };
      break;
    case VERIFY_TRANSACTION_ERROR:
      state = {
        ...state,
        VerifierError: action.payload,
        VerifierSuccess: null,
        VerifierLoading: false,
      };
      break;

    // Reset especifico
    case RESET_VERIFIER:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }
  return state;
};

export default Verifier;
