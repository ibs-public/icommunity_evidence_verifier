import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Verify Transaction
  VERIFY_TRANSACTION,
} from "./actionTypes";

// Actions
import {
  // Verify Transaction
  verifyTransactionSuccess,
  verifyTransactionError,
} from "./actions";

// Verifiers
/* import { verifier } from "helpers/verifiers/verifier"; */

import * as api from "api/verifier/verifier.service";

/* --- FUNCTIONS --- */
// Verify Transaction
function* verifyTransaction({ params: { data, integrity } }) {
  try {
    const response = yield call(api.verify, data, integrity);
    /* console.log("Response verifyTransaction",response) */
    if (response.status === 1) {
      if (response.data && response.data === true) {
        yield put(verifyTransactionSuccess("MsgOk[Successful Evidence Verification]"));
      } else {
        yield put(verifyTransactionSuccess("MsgOk[Failed Evidence Verification]"));
      }
    } else {
      yield put(verifyTransactionError(`MsgErr[Internal Error]`));
    }
  } catch (error) {
    yield put(verifyTransactionError(error));
  }
}

function* VerifierSaga() {
  yield takeEvery(VERIFY_TRANSACTION, verifyTransaction);
}

export default VerifierSaga;
