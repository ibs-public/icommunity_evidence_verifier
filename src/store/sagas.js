import { all } from "redux-saga/effects"

//public
import TransactionSaga from "./transactions/saga"
import ContentSaga from "./content/saga"
import VerifierSaga from "./verifier/saga"

export default function* rootSaga() {
  yield all([
    //public
    TransactionSaga(),
    ContentSaga(),
    VerifierSaga()
  ])
}
