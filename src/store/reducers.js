import { combineReducers } from "redux"

// Front
import Transaction from "./transactions/reducer"
import Content from "./content/reducer"
import Verifier from "./verifier/reducer"

const rootReducer = combineReducers({
  // public
  Transaction,
  Content,
  Verifier
})

export default rootReducer
