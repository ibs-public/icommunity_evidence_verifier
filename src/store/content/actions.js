import {
  // Get Transaction Content
  GET_TRANSACTION_CONTENT,
  GET_TRANSACTION_CONTENT_SUCCESS,
  GET_TRANSACTION_CONTENT_ERROR,
} from "./actionTypes"

// Get Transaction Content
export const getTransactionContent = (network, hash) => {
  return {
    type: GET_TRANSACTION_CONTENT,
    params: { network, hash },
  }
}

export const getTransactionContentSuccess = obj => ({
  type: GET_TRANSACTION_CONTENT_SUCCESS,
  payload: obj,
})

export const getTransactionContentError = error => ({
  type: GET_TRANSACTION_CONTENT_ERROR,
  payload: error,
})