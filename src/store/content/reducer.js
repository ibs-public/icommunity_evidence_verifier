import {
  // Get Transaction Content
  GET_TRANSACTION_CONTENT,
  GET_TRANSACTION_CONTENT_SUCCESS,
  GET_TRANSACTION_CONTENT_ERROR,
  // Reset especifico
  RESET_CONTENT,
  // Reset general
  RESET,
} from "./actionTypes";

const initialState = {
  ContentError: "",
  ContentSuccess: "",
  ContentLoading: false,
  ContentData: {},
};

const Content = (state = initialState, action) => {
  switch (action.type) {
    // Get Transaction Content
    case GET_TRANSACTION_CONTENT:
      state = {
        ...state,
        ContentLoading: true,
        ContentError: "",
        ContentSuccess: "",
      };
      break;

    case GET_TRANSACTION_CONTENT_SUCCESS:
      state = {
        ...state,
        ContentData: action.payload,
        ContentSuccess: "MsgOk[Transaction Content Received]",
        ContentLoading: false,
      };
      break;
    case GET_TRANSACTION_CONTENT_ERROR:
      state = {
        ...state,
        ContentError: action.payload,
        ContentSuccess: "",
        ContentLoading: false,
      };
      break;

    // Reset especifico
    case RESET_CONTENT:
      state = initialState;
      break;

    // Reset general
    case RESET:
      state = initialState;
      break;

    default:
      state = { ...state };
      break;
  }
  return state;
};

export default Content;
