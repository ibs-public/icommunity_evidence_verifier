import { takeEvery, put, call } from "redux-saga/effects";

// ActionTypes
import {
  // Get Transaction Content
  GET_TRANSACTION_CONTENT,
} from "./actionTypes";

// Actions
import {
  // Get Transaction Content
  getTransactionContentSuccess,
  getTransactionContentError,
} from "./actions";

// ETHERS
/* import { get_Certification_Content } from "api/transactions/ethers.config"; */
import * as api from "api/content/content.service";
// Get Transaction Content
function* getTransactionContent({ params: { network, hash } }) {
  try {
    const response = yield call(api.getCertificationContent, network, hash);
   /*  console.log("RESPONSE verifier getTransactionContent",response) */
    if (response.status === 1) {
      yield put(getTransactionContentSuccess(response));
    } else if(response.status < 0){
      yield put(getTransactionContentError(`MsgErr[${response.message}]`));
    } else{
      yield put(getTransactionContentError("MsgErr[Transaction Not Found]"));
    }
  } catch (error) {
    yield put(getTransactionContentError(error));
  }
}

function* ContentSaga() {
  yield takeEvery(GET_TRANSACTION_CONTENT, getTransactionContent);
}

export default ContentSaga;
